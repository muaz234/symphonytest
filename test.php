function showMax($values, $size) 
{ 
  //initialize function and compare values in array
    for ($i = 0; $i < $size; $i++) 
    { 
        for ($j = $i + 1; 
             $j < $size; $j++) 
        { 
            if ($values[$i] <= $values[$j]) 
                break; 
        }  
          
          //max value found, display it and space between them
        if ($j == $size) 
            echo($values[$i] . " ");  
        } 
} 
$values = [54, 23, 19, 61, 34, 41, 7, 42, 60, 59];
$size=sizeof($values);
showMax($values,$size);